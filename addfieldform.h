#ifndef ADDFIELDFORM_H
#define ADDFIELDFORM_H

#include <QWidget>
namespace Ui {
class AddFieldForm;
}
class AddFieldForm : public QWidget
{
    Q_OBJECT
public:
    explicit AddFieldForm(QWidget *parent = nullptr);
    ~AddFieldForm();
private:
    Ui::AddFieldForm *ui;
};

#endif // ADDFIELDFORM_H
