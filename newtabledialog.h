#ifndef NEWTABLEDIALOG_H
#define NEWTABLEDIALOG_H

#include <QDialog>
#include "addfieldform.h"

QT_BEGIN_NAMESPACE
namespace Ui { class NewTableDialog; }
QT_END_NAMESPACE

class NewTableDialog : public QDialog
{
    Q_OBJECT
public:
    NewTableDialog(QWidget *parent = nullptr);
    ~NewTableDialog();
private slots:
//    void on_addFields_combo_index_activated(int index);
private:
    Ui::NewTableDialog *ui;
    QAction *mActionField;
    QAction *mActionDeleteField;
    AddFieldForm *mNewField;
};
#endif // NEWTABLEDIALOG_H
