#include "newtabledialog.h"
#include "ui_newtabledialog.h"
#include <QAction>
#include <QComboBox>
#include <QRadioButton>
#include <QCheckBox>
#include <QPushButton>

NewTableDialog::NewTableDialog(QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::NewTableDialog)
{
    ui->setupUi(this);
    ui->tableWidget->setContextMenuPolicy(Qt::ActionsContextMenu);
    mActionField = new QAction(QIcon(":"), tr("Add"), this);
    mActionDeleteField = new QAction(QIcon(":"), tr("Remove"), this);
    ui->tableWidget->addActions({mActionField, mActionDeleteField});

    auto newComboBox = [&](QWidget *parent) {
        auto combo = new QComboBox(parent);
        combo->addItems({"INTEGER","DOUBLE","TEXT","DATE","TIME","BOOL","BLOB", "ADD FIELD"});
        return combo;
    };

//    connect(newComboBox, QOverload<int>::of(&QComboBox::activated),
//        [=](int index) { on_addFields_combo_index_activated(index); });

    connect(mActionField, &QAction::triggered, [&]() {
        int rowCount = ui->tableWidget->rowCount();
        ui->tableWidget->insertRow(rowCount);
        ui->tableWidget->setCellWidget(rowCount, 1, newComboBox(this));
        ui->tableWidget->setCellWidget(rowCount, 2, new QRadioButton(this));
        ui->tableWidget->setCellWidget(rowCount, 3, new QCheckBox(this));
        ui->tableWidget->setCellWidget(rowCount, 4, new QCheckBox(this));
        ui->tableWidget->show();
    });
    connect(mActionDeleteField, &QAction::triggered, [&]() {
        ui->tableWidget->removeRow(ui->tableWidget->currentRow());
    });
}

NewTableDialog::~NewTableDialog()
{
    delete ui;
}

